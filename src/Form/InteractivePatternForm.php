<?php

namespace Drupal\patternkit_devel\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\devel\DevelDumperManagerInterface;
use Drupal\patternkit\Asset\PatternDiscoveryInterface;
use Drupal\patternkit\Entity\Pattern;
use Drupal\patternkit\Entity\PatternInterface;
use Drupal\patternkit\Schema\ContextBuilder;
use Drupal\patternkit\Schema\SchemaFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Provides a Patternkit devel form.
 */
class InteractivePatternForm extends FormBase {

  /**
   * Number of rows for the textarea.
   */
  public const ROWS_NUMBER = 20;

  /**
   * The patternkit pattern discovery service.
   *
   * @var \Drupal\patternkit\Asset\PatternDiscoveryInterface
   */
  protected PatternDiscoveryInterface $patternDiscovery;

  /**
   * The devel dumper manager service.
   *
   * @var \Drupal\devel\DevelDumperManagerInterface
   */
  protected DevelDumperManagerInterface $develDumper;

  /**
   * The session service.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected SessionInterface $session;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The schema factory service for instantiating new schemas.
   *
   * @var \Drupal\patternkit\Schema\SchemaFactory
   */
  protected SchemaFactory $schemaFactory;

  /**
   * The schema context builder service.
   *
   * @var \Drupal\patternkit\Schema\ContextBuilder
   */
  protected ContextBuilder $contextBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);

    $instance->patternDiscovery = $container->get('patternkit.pattern.discovery');
    $instance->develDumper = $container->get('devel.dumper');
    $instance->session = $container->get('session');
    $instance->renderer = $container->get('renderer');
    $instance->schemaFactory = $container->get('patternkit.schema.schema_factory');

    // Set the context builder if it is available.
    if ($container->has('patternkit.schema.context_builder')) {
      $instance->contextBuilder = $container->get('patternkit.schema.context_builder');
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'patternkit_devel_interactive_pattern_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['#redirect'] = FALSE;
    $pattern = $this->session->get('patternkit_devel_pattern', NULL);
    $config = $this->session->get('patternkit_devel_config', '');
    $bypass_validation = $this->session->get('patternkit_devel_bypass_validation', FALSE);
    $error = $this->session->get('patternkit_devel_error', FALSE);

    // Assemble the selection options for patterns.
    $pattern_options = [];
    foreach ($this->patternDiscovery->getPatternDefinitions() as $namespace => $patterns) {
      $pattern_options[$namespace] = [];
      foreach ($patterns as $id => $definition) {
        $name = $definition['name'][0]['value'];
        $pattern_options[$namespace][$id] = sprintf('%s (%s)', $id, $name);
      }
    }

    $form['pattern'] = [
      '#type' => 'select',
      '#options' => $pattern_options,
      '#title' => $this->t('Pattern to render'),
      '#required' => TRUE,
      '#default_value' => $pattern,
    ];

    $form['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pattern configuration values'),
      '#title_display' => 'invisible',
      '#description' => $this->t('Enter JSON configuration for the selected pattern.'),
      '#default_value' => $config,
      '#rows' => self::ROWS_NUMBER,
      '#attributes' => [
        'style' => 'font-family: monospace; font-size: 1.25em;',
      ],
      '#required' => TRUE,
    ];

    $form['bypass_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bypass schema validation'),
      '#description' => $this->t("Don't validate configuration against the schema during form validation. This allows you to see the result of attempting to render the pattern with potentially invalid configuration."),
      '#default_value' => $bypass_validation,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute'),
    ];

    // If we have submission values, and it didn't cause an error, render the
    // output for previewing.
    if (isset($pattern) && isset($config) && !$error) {
      $form['output'] = [
        '#type' => 'container',
      ];

      $pattern_instance = $this->getPatternInstance($pattern);

      $form['output']['render_preview'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Render preview'),
      ];
      $form['output']['render_preview']['pattern'] = [
        '#type' => 'pattern',
        '#pattern' => $pattern_instance,
        '#config' => json_decode($config, TRUE),
      ];

      $form['output']['debug'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Debug output'),
      ];
      $form['output']['debug']['render_element'] = $this->develDumper->exportAsRenderable($form['output']['render_preview']['pattern']);
    }

    // Unset session values to start fresh with the new submission. These will
    // be repopulated in the validation and submission handlers.
    $session_values = [
      'patternkit_devel_pattern',
      'patternkit_devel_config',
      'patternkit_devel_bypass_validation',
      'patternkit_devel_error',
    ];
    foreach ($session_values as $value) {
      if ($this->session->has($value)) {
        $this->session->remove($value);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $config = $form_state->getValue('config');

    // Ensure the JSON configuration that was provided is valid.
    try {
      json_decode($config, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $jsonException) {
      $form_state->setErrorByName('config', $this->t('The provided configuration is not valid JSON.'));

      return;
    }

    if (!$form_state->getValue('bypass_validation')) {
      // Confirm the provided data validates for the selected pattern.
      $pattern = $form_state->getValue('pattern');
      $pattern_entity = $this->getPatternInstance($pattern);

      try {
        $this->validateSchemaConfig($pattern_entity, $config);
      }
      catch (\Exception $exception) {
        $form_state->setErrorByName('config', $this->t('The provided configuration does not validate against the pattern schema.'));
        $this->messenger()->addError($exception->getMessage());
        $this->develDumper->message($exception, get_class($exception), 'error');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $pattern = $form_state->getValue('pattern');
    $config = $form_state->getValue('config');
    $bypass_validation = $form_state->getValue('bypass_validation');

    $pattern_instance = $this->getPatternInstance($pattern);

    $element = [
      '#type' => 'pattern',
      '#pattern' => $pattern_instance,
      '#config' => json_decode($config, JSON_OBJECT_AS_ARRAY),
    ];

    try {
      \ob_start();
      $this->renderer->render($element);

      if ($error_output = \ob_get_clean()) {
        $this->session->set('patternkit_devel_error', TRUE);
        $this->develDumper->message($error_output);
      }
    }
    catch (\Throwable $error) {
      $this->messenger()->addError($error->getMessage());
      $this->session->set('patternkit_devel_error', TRUE);
    }

    $this->session->set('patternkit_devel_pattern', $pattern);
    // Decode and re-encode with formatting.
    $this->session->set('patternkit_devel_config', json_encode(json_decode($config), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    $this->session->set('patternkit_devel_bypass_validation', $bypass_validation);
  }

  /**
   * Create an instance of a specified pattern.
   *
   * @param string $pattern_id
   *   The namespaced identifier for the pattern to load.
   *
   * @return \Drupal\patternkit\Entity\Pattern
   *   A loaded pattern entity for the specified pattern.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getPatternInstance(string $pattern_id): Pattern {
    $pattern_definition = $this->patternDiscovery->getPatternDefinition($pattern_id);
    return Pattern::create($pattern_definition);
  }

  /**
   * Validate provided pattern configuration against a pattern's schema.
   *
   * Throws an exception if validation fails.
   *
   * @param \Drupal\patternkit\Entity\PatternInterface $pattern
   *   The pattern entity to be validated against.
   * @param string $config
   *   The JSON configuration value to be validated.
   *
   * @return true
   *   Returns true if validation succeeds. Throws an exception if not.
   *
   * @throws \Swaggest\JsonSchema\InvalidValue
   */
  protected function validateSchemaConfig(PatternInterface $pattern, string $config): bool {
    $pattern_schema = $pattern->getSchema();
    $schema = $this->schemaFactory->createInstance($pattern_schema);

    if (isset($this->contextBuilder)) {
      $context = $this->contextBuilder->getDefaultContext();
    }
    else {
      $context = $this->schemaFactory->getDefaultContext();
    }

    $schema->in(json_decode($config), $context);

    return TRUE;
  }

}
