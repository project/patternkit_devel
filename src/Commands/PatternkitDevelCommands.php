<?php

namespace Drupal\patternkit_devel\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drush\Commands\DrushCommands;

/**
 * Commands for helpful development tasks with Patternkit.
 */
class PatternkitDevelCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Constructs the Patternkit Devel Drush Commands functionality.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The active database connection.
   */
  public function __construct(
    Connection $database
  ) {
    parent::__construct();

    $this->database = $database;
  }

  /**
   * Roll back updates for Patternkit blocks.
   *
   * This can be helpful for testing the update process of blocks.
   *
   * @return string
   *   A confirmation message for completion.
   *
   * @command patternkit:rollback
   * @aliases pkrb
   * @format string
   *
   * @usage drush pkrb
   *   Rollback updates in all blocks and layouts.
   */
  public function rollback(): string {
    // Update base table version values.
    $this->database->update('pattern')
      ->fields(['version' => 'OLD'])
      ->execute();

    // Update referenced revision table version values.
    $revisions = $this->database->select('pattern', 'p')
      ->fields('p', ['revision'])
      ->execute()
      ->fetchCol();
    $query = $this->database->update('pattern_revision')
      ->fields([
        'hash' => 'ALTERED',
        'version' => 'OLD',
      ])
      ->condition('revision', $revisions, 'IN');

    $updated = $query->execute();

    return $this->t('Updated @updated pattern revisions.', [
      '@updated' => $updated,
    ]);
  }

}
