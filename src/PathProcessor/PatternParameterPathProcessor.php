<?php

namespace Drupal\patternkit_devel\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * A path processor implementation to handle identification of pattern ids.
 *
 * Since pattern identifiers contain slashes, the routing system doesn't
 * support recognition of them as a single parameter value. To work around this
 * shortcoming, this processor identifies affected paths and replaces the
 * separators in the pattern identifier with a ':' instead which will allow for
 * matching as a single parameter. This allows the route matching to
 * successfully identify the route to send the request to, but before the
 * controller may use the pattern identifier, this replacement process must be
 * reversed.
 *
 * Controllers receiving encoded parameter values should use the
 * decodePatternIdentifier() method to restore the original pattern ID value.
 *
 * @see \Drupal\patternkit_devel\PathProcessor\PatternParameterPathProcessor::decodePatternIdentifier()
 */
class PatternParameterPathProcessor implements InboundPathProcessorInterface {

  /**
   * The path prefix to match requests for considerations.
   *
   * @var string
   */
  const DEVEL_PATH_PREFIX = '/devel/patterns/';

  /**
   * The placeholder token to replace '/' with in pattern identifiers.
   *
   * @var string
   */
  const PLACEHOLDER_TOKEN = ':';

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    if (strpos($path, $this::DEVEL_PATH_PREFIX) === 0) {
      $prefix = strstr($path, '@', TRUE);
      $path = static::encodePatternIdentifier($prefix, $path);
    }

    return $path;
  }

  /**
   * Replace pattern ID separators with a ':' to allow route matching.
   *
   * @param string $prefix
   *   The route prefix to ignore and exclude from replacements.
   * @param string $path
   *   The full path to be rewritten.
   *
   * @return string
   *   The path value after replacing all '/' characters in the pattern
   *   identifier with ':' instead.
   */
  public static function encodePatternIdentifier(string $prefix, string $path): string {
    // Trim the prefix from the path to get the pattern identifier.
    $pattern_id = substr($path, strlen($prefix));

    // Replace the slashes in the pattern identifier so this path properly
    // matches the pattern identifier as a single parameter for routing.
    $escaped_pattern_id = strtr($pattern_id, '/', static::PLACEHOLDER_TOKEN);

    return $prefix . $escaped_pattern_id;
  }

  /**
   * A helper function to reverse the encoding of the pattern identifier.
   *
   * This is a helper function for controllers receiving an encoded parameter.
   * It may be used to reverse the encoding applied during route processing and
   * restore the raw pattern identifier value.
   *
   * @param string $pattern_id
   *   The encoded pattern identifier to be decoded.
   *
   * @return string
   *   The decoded pattern identifier.
   */
  public static function decodePatternIdentifier(string $pattern_id): string {
    return strtr($pattern_id, static::PLACEHOLDER_TOKEN, '/');
  }

}
