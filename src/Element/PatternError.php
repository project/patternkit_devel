<?php

namespace Drupal\patternkit_devel\Element;

use Drupal\patternkit\Element\PatternError as PatternkitPatternError;

/**
 * Overrides the default pattern error element to enhance debug information.
 *
 * @see \Drupal\patternkit\Element\PatternError
 * @see \patternkit_devel_element_plugin_alter()
 */
class PatternError extends PatternkitPatternError {

  /**
   * {@inheritdoc}
   */
  public function preRenderDebugOutput(array $element): array {
    $element = parent::preRenderDebugOutput($element);

    // Skip altogether if the user doesn't have access to dev output.
    if (!$this->shouldDisplayDebugOutput()) {
      return $element;
    }

    // Output additional debugging context to search through if a variable
    // dumping service is available.
    if (\Drupal::hasService('devel.dumper')) {
      $dumper = \Drupal::service('devel.dumper');

      if (isset($element['#exception'])) {
        $element['debug']['exception'] = $dumper->exportAsRenderable($element['#exception'], 'Exception');
      }

      $element['debug']['render'] = $dumper->exportAsRenderable([
        'pattern' => $element['#pattern'],
        'config' => $element['#config'],
        'config_json' => json_encode($element['#config'], JSON_PRETTY_PRINT),
        'context' => $element['#context'],
      ], 'Render content');
    }

    return $element;
  }

}
