<?php

namespace Drupal\patternkit_devel\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\devel\DevelDumperManagerInterface;
use Drupal\patternkit\Asset\LibraryNamespaceResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides route responses for the pattern info page.
 */
class LibraryInfoController extends ControllerBase {

  /**
   * The library namespace resolver service.
   *
   * @var \Drupal\patternkit\Asset\LibraryNamespaceResolverInterface
   */
  protected LibraryNamespaceResolverInterface $resolver;

  /**
   * The dumper service.
   *
   * @var \Drupal\devel\DevelDumperManagerInterface
   */
  protected DevelDumperManagerInterface $dumper;

  /**
   * LibraryInfoController constructor.
   *
   * @param \Drupal\patternkit\Asset\LibraryNamespaceResolverInterface $resolver
   *   The library namespace resolver service.
   * @param \Drupal\devel\DevelDumperManagerInterface $dumper
   *   The dumper service.
   */
  public function __construct(LibraryNamespaceResolverInterface $resolver, DevelDumperManagerInterface $dumper) {
    $this->resolver = $resolver;
    $this->dumper = $dumper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('patternkit.library.namespace_resolver'),
      $container->get('devel.dumper')
    );
  }

  /**
   * Builds the libraries overview page.
   *
   * @return array
   *   A render array as expected by the renderer.
   */
  public function libraryList(): array {
    $headers = [
      $this->t('Name'),
      $this->t('Provider'),
      $this->t('Operations'),
    ];

    $rows = [];

    $info = $this->resolver->getLibraryDefinitions();

    foreach ($info as $namespace => $definition) {
      $row['name'] = [
        'data' => $namespace,
        'filter' => TRUE,
      ];
      $row['provider'] = [
        'data' => $definition['extension'],
        'filter' => TRUE,
      ];
      $row['operations']['data'] = [
        '#type' => 'operations',
        '#links' => [
          'devel' => [
            'title' => $this->t('Devel'),
            'url' => Url::fromRoute('patternkit_devel.libraries_page.detail', ['library_name' => $namespace]),
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => 700,
                'minHeight' => 500,
              ]),
            ],
          ],
        ],
      ];

      $rows[$namespace] = $row;
    }

    ksort($rows);

    $output['libraries'] = [
      '#type' => 'devel_table_filter',
      '#filter_label' => $this->t('Search'),
      '#filter_placeholder' => $this->t('Enter library id, namespace, or provider'),
      '#filter_description' => $this->t('Enter a part of the library id, namespace, or provider to filter by.'),
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No libraries found.'),
      '#sticky' => TRUE,
      '#attributes' => [
        'class' => ['devel-library-list'],
      ],
    ];

    return $output;
  }

  /**
   * Returns a render array representation of the library.
   *
   * @param string $library_name
   *   The name of the library to retrieve.
   *
   * @return array
   *   A render array containing the library info.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   If the requested library is not defined.
   */
  public function libraryDetail(string $library_name): array {
    try {
      if ($library_name[0] === '@') {
        $definition = $this->resolver->getLibraryFromNamespace($library_name);
      }
      else {
        $definition = NULL;
      }
    }
    catch (\Exception $exception) {
      $definition = NULL;
    }

    if (!$definition) {
      throw new NotFoundHttpException();
    }

    return $this->dumper->exportAsRenderable($definition, $library_name);
  }

}
