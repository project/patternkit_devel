<?php

namespace Drupal\patternkit_devel\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\devel\DevelDumperManagerInterface;
use Drupal\patternkit\Asset\PatternDiscoveryInterface;
use Drupal\patternkit\Entity\Pattern;
use Drupal\patternkit_devel\PathProcessor\PatternParameterPathProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides route responses for the pattern info page.
 */
class PatternInfoController extends ControllerBase {

  /**
   * The pattern discovery service.
   *
   * @var \Drupal\patternkit\Asset\PatternDiscoveryInterface
   */
  protected PatternDiscoveryInterface $patternDiscovery;

  /**
   * The dumper service.
   *
   * @var \Drupal\devel\DevelDumperManagerInterface
   */
  protected DevelDumperManagerInterface $dumper;

  /**
   * PatternInfoController constructor.
   *
   * @param \Drupal\patternkit\Asset\PatternDiscoveryInterface $patternDiscovery
   *   The pattern discovery service.
   * @param \Drupal\devel\DevelDumperManagerInterface $dumper
   *   The dumper service.
   */
  public function __construct(PatternDiscoveryInterface $patternDiscovery, DevelDumperManagerInterface $dumper) {
    $this->patternDiscovery = $patternDiscovery;
    $this->dumper = $dumper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('patternkit.pattern.discovery'),
      $container->get('devel.dumper'),
    );
  }

  /**
   * Builds the pattern overview page.
   *
   * @return array
   *   A render array as expected by the renderer.
   */
  public function patternList(): array {
    $headers = [
      $this->t('Name'),
      $this->t('Namespace'),
      $this->t('Operations'),
    ];

    $rows = [];

    $info = $this->patternDiscovery->getPatternDefinitions();

    foreach ($info as $namespace => $definitions) {
      foreach ($definitions as $name => $definition) {
        $row['name'] = [
          'data' => $name,
          'filter' => TRUE,
        ];
        $row['namespace'] = [
          'data' => $namespace,
          'filter' => TRUE,
        ];
        $row['operations']['data'] = [
          '#type' => 'operations',
          '#links' => [
            'devel' => [
              'title' => $this->t('Devel'),
              'url' => Url::fromRoute('patternkit_devel.patterns_page.detail', ['pattern_name' => $name]),
              'attributes' => [
                'class' => ['use-ajax'],
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode([
                  'width' => 700,
                  'minHeight' => 500,
                ]),
              ],
            ],
          ],
        ];

        $rows[$name] = $row;
      }
    }

    ksort($rows);

    $output['patterns'] = [
      '#type' => 'devel_table_filter',
      '#filter_label' => $this->t('Search'),
      '#filter_placeholder' => $this->t('Enter pattern name or namespace'),
      '#filter_description' => $this->t('Enter a part of the pattern name or namespace to filter by.'),
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No patterns found.'),
      '#sticky' => TRUE,
      '#attributes' => [
        'class' => ['devel-pattern-list'],
      ],
    ];

    return $output;
  }

  /**
   * Returns a render array representation of the pattern.
   *
   * @param string $pattern_name
   *   The name of the pattern to retrieve.
   *
   * @return array
   *   A render array containing the pattern info.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   If the requested pattern is not defined.
   */
  public function patternDetail(string $pattern_name): array {
    // Replace placeholder separators from path processing back to slashes to
    // enable matches against discovered pattern data.
    // @see \Drupal\patternkit_devel\PathProcessor\PatternParameterPathProcessor::processInbound
    $pattern_id = PatternParameterPathProcessor::decodePatternIdentifier($pattern_name);
    if (!$definition = $this->patternDiscovery->getPatternDefinition($pattern_id)) {
      throw new NotFoundHttpException();
    }

    // Include status messages in case an exception happens.
    $output['messages'] = [
      '#type' => 'status_messages',
    ];
    $output['definition'] = $this->dumper->exportAsRenderable($definition, $this->t('Pattern definition'));

    try {
      $pattern = Pattern::create($definition);

      $output['entity'] = $this->dumper->exportAsRenderable($pattern, $this->t('Pattern entity'));
      $output['schema'] = $this->dumper->exportAsRenderable($pattern->getSchema(), $this->t('Pattern schema'));
      $output['template'] = $this->dumper->exportAsRenderable($pattern->getTemplate(), $this->t('Pattern template'));

    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->messenger()->addError($e->getMessage());
    }

    return $output;
  }

  /**
   * Title callback for the pattern detail page.
   *
   * This is needed to decode pattern name encoding added in path processing.
   *
   * @param string $pattern_name
   *   The pattern name parameter to be decoded.
   *
   * @return string
   *   The page title.
   */
  public function patternDetailTitle(string $pattern_name): string {
    // Replace placeholder separators from path processing back to slashes to
    // enable matches against discovered pattern data.
    // @see \Drupal\patternkit_devel\PathProcessor\PatternParameterPathProcessor::processInbound
    $pattern_id = PatternParameterPathProcessor::decodePatternIdentifier($pattern_name);

    return $this->t('Pattern @pattern', ['@pattern' => $pattern_id]);
  }

}
