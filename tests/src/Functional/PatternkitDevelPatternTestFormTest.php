<?php

namespace Drupal\Tests\patternkit_devel\Functional;

/**
 * Tests pattern test form behavior.
 *
 * @group patternkit_devel
 */
class PatternkitDevelPatternTestFormTest extends PatternkitDevelBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_menu_block:devel');
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalLogin($this->develUser);
  }

  /**
   * Tests pattern test menu link.
   */
  public function testPatternTestMenuLink() {
    $this->drupalPlaceBlock('system_menu_block:devel');
    // Ensures that the pattern test link is present on the devel menu and that
    // it points to the correct page.
    $this->drupalGet('');
    $this->clickLink('Pattern Test');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/devel/pattern-test');
    $this->assertSession()->pageTextContains('Interactive Pattern Test');
  }

  /**
   * Tests pattern test form page.
   */
  public function testPatternTestForm() {
    $this->drupalGet('/devel/pattern-test');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Interactive Pattern Test');

    $page = $this->getSession()->getPage();

    // Ensures the pattern selection field is found.
    $select = $page->findField('pattern');
    $this->assertNotNull($select, 'Expected to find a pattern selection field.');

    // Ensures known patterns are available selection options.
    $expected_patterns = [
      '@patternkit/atoms/example/src/example',
      '@patternkit/atoms/example_filtered/src/example_filtered',
      '@patternkit/atoms/example_ref/src/example_ref',
    ];
    foreach ($expected_patterns as $pattern_id) {
      $option = $select->find('css', sprintf('option[value="%s"]', $pattern_id));
      $this->assertNotNull($option, "Unable to find expected pattern option '$pattern_id'.");
    }

    // Ensures the config field is found.
    $this->assertTrue($page->hasField('config'), 'Expected to find a pattern configuration field.');

    // Ensures the bypass validation field is found.
    $this->assertTrue($page->hasField('bypass_validation'), 'Expected to find a "bypass_validation" field.');

    // Ensures that the page is accessible only to the users with the adequate
    // permissions.
    $this->drupalLogout();
    $this->drupalGet('devel/pattern-test');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests valid pattern config submissions.
   */
  public function testValidPatternSubmission() {
    $pattern_name = '@patternkit/atoms/example/src/example';
    $pattern_config = <<<JSON
      {
        "text": "My text value",
        "formatted_text": "<strong>Some super <em>fancy</em> text</strong>",
        "hidden": "A secret message"
      }
      JSON;

    // Ensures that the page works as expected.
    $this->drupalGet("/devel/pattern-test");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Interactive Pattern Test");

    $page = $this->getSession()->getPage();

    $page->selectFieldOption('pattern', $pattern_name);
    // Mangle the submitted config to test formatting cleanup.
    $page->fillField('config', json_encode(json_decode($pattern_config)));

    $page->pressButton('Execute');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/devel/pattern-test');

    // Ensure submission values were persisted.
    $this->assertSession()->fieldValueEquals('pattern', $pattern_name);
    $this->assertSession()->fieldValueEquals('config', json_encode(json_decode($pattern_config), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

    // Expect no status message output.
    $this->assertSession()->statusMessageNotExists();

    // Expect the rendered output to be visible.
    $this->assertSession()->elementExists('css', '#edit-render-preview');
    $this->assertSession()->elementTextContains('css', '#edit-render-preview', "Sample twig template.");
    $this->assertSession()->elementTextContains('css', '#edit-render-preview', "My text value");
    $this->assertSession()->elementTextContains('css', '#edit-render-preview', "Some super fancy text");

    // Expect debug output to be available.
    $this->assertSession()->elementExists('css', '#edit-debug');
  }

  /**
   * Tests submission with invalid JSON.
   */
  public function testInvalidJsonSubmission() {
    $pattern_name = '@patternkit/atoms/example/src/example';
    $pattern_config = <<<JSON
      [
        This isn't JSON
      }
      JSON;

    // Ensures that the page works as expected.
    $this->drupalGet("/devel/pattern-test");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Interactive Pattern Test");

    $page = $this->getSession()->getPage();

    $page->selectFieldOption('pattern', $pattern_name);
    $page->fillField('config', $pattern_config);

    $page->pressButton('Execute');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/devel/pattern-test');

    // Expect an error message warning about the invalid value.
    $this->assertSession()->statusMessageContains('The provided configuration is not valid JSON.', 'error');

    // Expect the config field to be marked with an error.
    $this->assertSession()->elementAttributeContains('css', '#edit-config', 'class', 'error');
  }

}
