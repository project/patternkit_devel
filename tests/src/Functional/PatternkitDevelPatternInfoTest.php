<?php

namespace Drupal\Tests\patternkit_devel\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Core\Url;

/**
 * Tests pattern info pages and links.
 *
 * @group patternkit_devel
 */
class PatternkitDevelPatternInfoTest extends PatternkitDevelBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_menu_block:devel');
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalLogin($this->develUser);
  }

  /**
   * Tests pattern info menu link.
   */
  public function testPatternInfoMenuLink() {
    $this->drupalPlaceBlock('system_menu_block:devel');
    // Ensures that the pattern info link is present on the devel menu and that
    // it points to the correct page.
    $this->drupalGet('');
    $this->clickLink('Patterns Info');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/devel/patterns');
    $this->assertSession()->pageTextContains('Pattern Info');
  }

  /**
   * Tests pattern list page.
   */
  public function testPatternList() {
    $this->drupalGet('/devel/patterns');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Pattern Info');

    $page = $this->getSession()->getPage();

    // Ensures that the library list table is found.
    $table = $page->find('css', 'table.devel-pattern-list');
    $this->assertNotNull($table);

    // Ensures that the expected table headers are found.
    $headers = $table->findAll('css', 'thead th');
    $this->assertEquals(3, count($headers));

    $expected_headers = ['Name', 'Namespace', 'Operations'];
    $actual_headers = array_map(function (NodeElement $element) {
      return $element->getText();
    }, $headers);
    $this->assertSame($expected_headers, $actual_headers);

    // Tests the presence of some (arbitrarily chosen) libraries in the table.
    $expected_patterns = [
      '@patternkit/atoms/example/src/example' => [
        'namespace' => '@patternkit',
      ],
      '@patternkit/atoms/example_filtered/src/example_filtered' => [
        'namespace' => '@patternkit',
      ],
      '@patternkit/atoms/example_ref/src/example_ref' => [
        'namespace' => '@patternkit',
      ],
    ];

    foreach ($expected_patterns as $pattern_name => $pattern) {
      $row = $table->find('css', sprintf('tbody tr:contains("%s")', $pattern_name));
      $this->assertNotNull($row);

      /** @var \Behat\Mink\Element\NodeElement[] $cells */
      $cells = $row->findAll('css', 'td');
      $this->assertEquals(3, count($cells));

      $cell = $cells[0];
      $this->assertEquals($pattern_name, $cell->getText());
      $this->assertTrue($cell->hasClass('table-filter-text-source'));

      $cell = $cells[1];
      $this->assertEquals($pattern['namespace'], $cell->getText());
      $this->assertTrue($cell->hasClass('table-filter-text-source'));

      $cell = $cells[2];
      $actual_href = $cell->findLink('Devel')->getAttribute('href');
      $expected_href = Url::fromRoute('patternkit_devel.patterns_page.detail', ['pattern_name' => $pattern_name])->toString();
      $this->assertEquals($expected_href, $actual_href);
    }

    // Ensures that the page is accessible only to the users with the adequate
    // permissions.
    $this->drupalLogout();
    $this->drupalGet('devel/patterns');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests pattern detail page.
   */
  public function testPatternDetail() {
    $pattern_name = '@patternkit/atoms/example/src/example';

    // Ensures that the page works as expected.
    $this->drupalGet("/devel/patterns/$pattern_name");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Pattern $pattern_name");

    // Ensures that the page returns a 404 error if the requested library is
    // not defined.
    $this->drupalGet('/devel/patterns/not_exists');
    $this->assertSession()->statusCodeEquals(404);

    // Ensures that the page is accessible ony to users with the adequate
    // permissions.
    $this->drupalLogout();
    $this->drupalGet("/devel/patterns/$pattern_name");
    $this->assertSession()->statusCodeEquals(403);
  }

}
