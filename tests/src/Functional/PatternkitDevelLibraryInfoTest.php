<?php

namespace Drupal\Tests\patternkit_devel\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\Core\Url;

/**
 * Tests library info pages and links.
 *
 * @group patternkit_devel
 */
class PatternkitDevelLibraryInfoTest extends PatternkitDevelBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_menu_block:devel');
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalLogin($this->develUser);
  }

  /**
   * Tests library info menu link.
   */
  public function testLibraryInfoMenuLink() {
    $this->drupalPlaceBlock('system_menu_block:devel');
    // Ensures that the library info link is present on the devel menu and that
    // it points to the correct page.
    $this->drupalGet('');
    $this->clickLink('Libraries Info');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('/devel/libraries');
    $this->assertSession()->pageTextContains('Library Info');
  }

  /**
   * Tests library list page.
   */
  public function testLibraryList() {
    $this->drupalGet('/devel/libraries');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Library Info');

    $page = $this->getSession()->getPage();

    // Ensures that the library list table is found.
    $table = $page->find('css', 'table.devel-library-list');
    $this->assertNotNull($table);

    // Ensures that the expected table headers are found.
    $headers = $table->findAll('css', 'thead th');
    $this->assertEquals(3, count($headers));

    $expected_headers = ['Name', 'Provider', 'Operations'];
    $actual_headers = array_map(function (NodeElement $element) {
      return $element->getText();
    }, $headers);
    $this->assertSame($expected_headers, $actual_headers);

    // Tests the presence of some (arbitrarily chosen) libraries in the table.
    $expected_libraries = [
      '@drupal.system' => [
        'provider' => 'system',
      ],
      '@drupal.text' => [
        'provider' => 'text',
      ],
      '@patternkit' => [
        'provider' => 'patternkit_example',
      ],
    ];

    foreach ($expected_libraries as $library_name => $library) {
      $row = $table->find('css', sprintf('tbody tr:contains("%s")', $library_name));
      $this->assertNotNull($row);

      /** @var \Behat\Mink\Element\NodeElement[] $cells */
      $cells = $row->findAll('css', 'td');
      $this->assertEquals(3, count($cells));

      $cell = $cells[0];
      $this->assertEquals($library_name, $cell->getText());
      $this->assertTrue($cell->hasClass('table-filter-text-source'));

      $cell = $cells[1];
      $this->assertEquals($library['provider'], $cell->getText());
      $this->assertTrue($cell->hasClass('table-filter-text-source'));

      $cell = $cells[2];
      $actual_href = $cell->findLink('Devel')->getAttribute('href');
      $expected_href = Url::fromRoute('patternkit_devel.libraries_page.detail', ['library_name' => $library_name])->toString();
      $this->assertEquals($expected_href, $actual_href);
    }

    // Ensures that the page is accessible only to the users with the adequate
    // permissions.
    $this->drupalLogout();
    $this->drupalGet('devel/libraries');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests library detail page.
   */
  public function testLibraryDetail() {
    $library_name = '@patternkit';

    // Ensures that the page works as expected.
    $this->drupalGet("/devel/libraries/$library_name");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Library $library_name");

    // Ensures that the page returns a 404 error if the requested library is
    // not defined.
    $this->drupalGet('/devel/libraries/not_exists');
    $this->assertSession()->statusCodeEquals(404);

    // Ensures that the page is accessible ony to users with the adequate
    // permissions.
    $this->drupalLogout();
    $this->drupalGet("/devel/libraries/$library_name");
    $this->assertSession()->statusCodeEquals(403);
  }

}
