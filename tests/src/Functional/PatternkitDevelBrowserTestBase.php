<?php

namespace Drupal\Tests\patternkit_devel\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Browser test base class for Patternkit Devel functional tests.
 */
abstract class PatternkitDevelBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'patternkit',
    'patternkit_example',
    'patternkit_devel',
    'devel',
    'block',
  ];

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * User with Devel access but not site admin permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $develUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access devel information',
      'administer site configuration',
    ]);

    $this->develUser = $this->drupalCreateUser([
      'access devel information',
    ]);
  }

}
