# Patternkit Devel Module

The Patternkit Devel module is intended as a local development utility to
supplement functionality of the Patternkit module and any related modules
depending on it. The primary goals of this module are to:

- Improve developer experience when working with Patternkit functionality
- Expose additional debugging information to ease troubleshooting issues with
  Patternkit functionality
- Provide tools to inspect discovery data and understand library and pattern
  discovery
- Provide tools to shortcut the process of testing patterns and related
  configuration data

## Dependencies

The Patternkit Devel module is dependent on a compatible version of the
[Patternkit module][], and tools from the [Devel module][]. The recommended
version for the Patternkit module is version 9.1.0-beta4 or greater. Significant
changes to available services were introduced in this release, and the tools
provided by this module are dependent on those new services being available.

### Suggested dependencies

The [Devel Kint Extras module][] is also recommended to improve utility of the
data dumped for review.

## Installation

The Patternkit Devel module may be installed like any other Drupal module using
the following command:

```bash
composer require --dev drupal/patternkit_devel
```

Marking the module as a dev dependency is recommended, since this module should
not be released and enabled in Production environments.

After installing the module, it may also be enabled like any other Drupal module
using either the administrative extensions page, or the following Drush command:

```bash
drush pm-enable patternkit_devel -y
```

## Features

The Patternkit Devel module provides a variety of features to achieve the goals
of making developer's lives easier.

### Library listing (`/devel/libraries`)

A collection of all library data processed by the Patternkit module may be
viewed and inspected at `/devel/libraries`. From this page, you'll see a
searchable listing of all discovered libraries and their providing modules.
For each of these libraries, you may also inspect the full data captured for
it by clicking the "Devel" button for that row. This should display a modal
window with a data dump of the full library information for review.

### Pattern listing (`/devel/patterns`)

A collection of all pattern data discovered and processed by the Patternkit
module may be viewed and inspected at `/devel/patterns`. From this page
you'll see a searchable listing of all discovered patterns and other related
data captured about them. For more detail, each pattern definition may be
viewed in full by clicking the "Devel" button for that row. This should display
a modal window with a data dump of the full pattern definition for review.

### Interactive pattern testing form (`/devel/pattern-test`)

An interactive testing form is available for testing the rendering process
of discovered patterns using manually provided configuration data in the form
of JSON. This enables active testing of specific patterns that may be under
development without having to worry about caching at various levels, including
rendering or the database caching of Patternkit blocks.

This form allows for the selection of a discovered pattern, specification
of JSON configuration data, and whether to validate the configuration data
against the pattern's schema during validation or to pass it through
regardless to the rendering phase.

After a submission, the following output is made available for review:

- The rendered output of the pattern with the provided configuration data.
- Any exception data in the case of a render failure.
- A dump of the assembled render element including:
  - The created pattern entity instance.
  - The decoded configuration data.

In the case of a rendering or validation failure, any text output is also
provided back to the user through a status message.

### Additional error debug output

The default pattern error output provided from the Patternkit module is
overridden to provide additional debugging context to privileged users. When
available, the dumped debug output is expanded to include a full dump of the
captured exception. This can be helpful for debugging as it often contains
more context to pinpoint the cause of an exception including the path and
schema content where a validation failure may have occurred.

[Patternkit module]: https://www.drupal.org/project/patternkit

[Devel module]: https://www.drupal.org/project/devel

[Devel Kint Extras module]: https://www.drupal.org/project/devel_kint_extras
